package formatter

import (
	"go-mod.ewintr.nl/adoc/document"
	"go-mod.ewintr.nl/adoc/element"
)

type Formatter interface {
	Format(doc *document.Document) string
	FormatFragments(els ...element.Element) string
}
