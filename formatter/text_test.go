package formatter_test

import (
	"strings"
	"testing"

	"go-mod.ewintr.nl/adoc/formatter"
	"go-mod.ewintr.nl/adoc/parser"
	"go-mod.ewintr.nl/go-kit/test"
)

func TestText(t *testing.T) {
	input := `= A Title

Some document

With some text.`

	exp := `A Title

Some document

With some text.

`

	doc := parser.New(strings.NewReader(input)).Parse()
	test.Equals(t, exp, formatter.NewText().Format(doc))
}
