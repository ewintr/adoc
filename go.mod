module go-mod.ewintr.nl/adoc

go 1.23.0

require (
	go-mod.ewintr.nl/go-kit v0.0.0-20240915084633-589e6c3a4b97 // indirect
	golang.org/x/text v0.14.0 // indirect
)
